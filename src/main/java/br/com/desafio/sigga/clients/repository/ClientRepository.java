package br.com.desafio.sigga.clients.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.desafio.sigga.clients.entity.Client;

/**
 * 
 * @author pedro.carvalho
 * @date 29/09/2020
 * 
 */

@RepositoryRestResource(path = "client", collectionResourceRel = "clients")
public interface ClientRepository extends JpaRepository<Client, Long>, JpaSpecificationExecutor<Client> {
	
}