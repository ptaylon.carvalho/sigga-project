package br.com.desafio.sigga.clients.controller;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.ResponseStatusException;

import br.com.desafio.sigga.clients.utils.CopyPropertiesUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author pedro.carvalho
 * @date 29/09/2020
 * 
 * 
 * Classe responsavel por abstrair as principais operacoes nas entidades:
 * 
 * 		PUT  
 *  	GET  
 *  	POST 
 *  	PATH 
 *  	DELETE
 *  
 * @param <T>
 * @param <ID>
 * @param <REPOSITORY_TYPE>
 */

@Data
@Slf4j
public abstract class AbstractRestController<T, D, ID extends Serializable, R extends JpaRepository<T, ID>> {

	@Autowired
	private R repo;

	@Autowired
	private ModelMapper modelMapper;

	final Class<D> dtoParameterClass;
	
	/**
	 * Construtor capturando classe parametrizada em runtime para conversao dos DTO's.
	 */
	@SuppressWarnings("unchecked")
	public AbstractRestController() {
		this.dtoParameterClass = (Class<D>) ((ParameterizedType) getClass().getGenericSuperclass())
				.getActualTypeArguments()[1];
	}

	/**
	 * Lista todos os objetos cadastrados no banco de dados de acordo com o
	 * repostirio da entidade e realiza o parser para DTO informado.
	 * 
	 * @return
	 */
	@GetMapping
	public @ResponseBody ResponseEntity<Page<D>> listAll(Pageable pageable) {

		Page<T> pageEntity = repo.findAll(pageable);
		List<D> allDtos = pageEntity.getContent()
				  .stream()
				  .map(
					 entity -> modelMapper.map(entity, dtoParameterClass)
				  ).collect(Collectors.toList());
		
		return new ResponseEntity<>(new PageImpl<>(allDtos, pageable, pageEntity.getTotalPages()), HttpStatus.OK);

	}

	/**
	 * Cria um novo objeto no banco de dados de acordo com a entidade enviada no
	 * json
	 * 
	 * @param json
	 * @return
	 */
	@PostMapping(consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody ResponseEntity<D> save(@RequestBody T json) {

		log.debug("create() with body {} of type {}", json, json.getClass());
		T entity = repo.save(json);
		return new ResponseEntity<>(modelMapper.map(entity, dtoParameterClass), HttpStatus.OK);

	}

	/**
	 * Buscando o objeto de acordo com o id passado como parametro no metodo.
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping(value = "/{id}")
	public @ResponseBody ResponseEntity<D> get(@PathVariable ID id) {

		T entity = repo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
		return new ResponseEntity<>(modelMapper.map(entity, dtoParameterClass), HttpStatus.OK);
	
	}

	/**
	 * Atualizando objeto passado como parametro, retornado caso sucesso mensagem de
	 * sucesso para servico/id atualizado e objeto processado apos atualizaco no
	 * banco de dados.
	 * 
	 * @param id
	 * @param json
	 * @return
	 * @throws BusinessException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	@PutMapping(value = "/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody ResponseEntity<D> update(@PathVariable(required = true, value = "id") ID id, @RequestBody D json) {
		
		log.debug("update() of id#{} with body {}", id, json);
		
		T updated = null;
		T entity = repo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
		
		BeanUtils.copyProperties(json, entity);				
		updated = repo.save(entity);
		log.debug("updated enitity: {}", updated);

		return new ResponseEntity<>(modelMapper.map(updated, dtoParameterClass), HttpStatus.OK);

	}
	
	/**
	 * Realiza alteracao da entidate T passando somente os atributos
	 * que serao utilizados para modificacao.
	 * 
	 * @param id
	 * @param json
	 * @return
	 */
	@PatchMapping(value="/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody ResponseEntity<D> patch(@PathVariable(required = true, value = "id") ID id, 
												 @RequestBody D json) {
		
		T entity = repo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
		CopyPropertiesUtil.copyNonNull(json, entity);
		return this.update(id, modelMapper.map(entity, dtoParameterClass));
				
	}
	
	/**
	 * Realiza a exclusao de um objeto de acordo com sua chave primaria do banco de
	 * dados, retornando mensagem de success caso tudo ocorra corretamente.
	 * 
	 * @param id
	 * @return
	 */
	@DeleteMapping(value="/{id}")
	public @ResponseBody ResponseEntity<String> delete(@PathVariable(required = true, value="id") ID id) {

		T entity = repo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
		log.debug("delete enitity: {}", entity);
		repo.deleteById(id);
		return new ResponseEntity<>("success", HttpStatus.OK);
	
	}
	
}