package br.com.desafio.sigga.clients.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.desafio.sigga.clients.entity.Client;
import br.com.desafio.sigga.clients.entity.ClientDTO;
import br.com.desafio.sigga.clients.repository.ClientRepository;
import br.com.desafio.sigga.specification.ClientSpecification;

/**
 * 
 * @author pedro.carvalho
 * @date 29/09/2020
 * 
 */

@RestController
@RequestMapping(value="/api/client")
public class ClientController extends AbstractRestController<Client, ClientDTO, Long, ClientRepository> {
	
	@GetMapping(value="/findClientByCpfOrName")
	public Page<Client> findClients(Pageable pageable,
						    @RequestParam(value = "cpf", required=false) String cpf,
						    @RequestParam(value = "client_name", required=false) String clientName) {
		
		return getRepo().findAll(
				new ClientSpecification(new Client(clientName, cpf)
			), pageable
		);
		
	}

} 