package br.com.desafio.sigga.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import br.com.desafio.sigga.clients.entity.Client;

/**
 * @author pedro.carvalho
 * @date 29/09/2020
 */
public class ClientSpecification implements Specification<Client> {
	
	private static final long serialVersionUID = -8696981875341656363L;
	
	private Client filter;

	public ClientSpecification(Client filter) {
		super();
		this.filter = filter;
	}

	/**
	 * To predicate.
	 *
	 * @param root the root
	 * @param criteriaQuery the criteria query
	 * @param criteriaBuilder the criteria builder
	 * @return the predicate
	 */
	public Predicate toPredicate(Root<Client> root, 
								 CriteriaQuery<?> criteriaQuery, 
								 CriteriaBuilder criteriaBuilder) {
		
		List<Predicate> predicateList = new ArrayList<>();
		Predicate predicate = criteriaBuilder.conjunction();

		if (filter.getName() != null) {
			predicateList.add(
				criteriaBuilder.like(
					criteriaBuilder.upper(root.get("name")), "%" + filter.getName().toUpperCase() + "%")
			);
		}
		
		if (filter.getCpf() != null) {
			predicateList.add(
				criteriaBuilder.equal(root.get("cpf"), filter.getCpf())
			);
		}
		
		if (!predicateList.isEmpty()) {
			predicate.getExpressions().add(
				criteriaBuilder.and(
					predicateList.toArray(new Predicate[predicateList.size()])));
		}

		return predicate;
	}
}